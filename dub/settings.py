# -*- coding: utf-8 -*-

# Scrapy settings for dub project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'dub'

SPIDER_MODULES = ['dub.spiders']
NEWSPIDER_MODULE = 'dub.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'dub (+http://www.yourdomain.com)'
USER_AGENT_LIST = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0) Gecko/16.0 Firefox/16.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'
]

NAMES = [
'Abaza',
'Abbas',
'AbdAllah',
'Abdeen',
'Abdoh',
'Abdul',
'Abdulla',
'Abed',
'Abiary',
'Abu',
'Adel',
'Adi',
'Adnan',
'Afifi',
'Afridi',
'Ahamed',
'Ahdab',
'Ahmadi',
'Ahmed',
'Aidy',
'Akhtar',
'Akkawi',
'Alanssi',
'Aldein',
'Ali',
'Aljabari',
'Badry',
'Bahadar',
'Bakhit',
'Balaa',
'Balama',
'Balbisi',
'Balli',
'Barow',
'Fauzi',
'Feraih',
'Fikry',
'Finianos',
'Folathi',
'Khouri',
'Khubara',
'Labib',
'Laham',
'Magdon',
'Maguid',
'Mahfoudh',
'Mogaz',
'Mohairbi',
'Shamisi',
'Shamloul',
'Shamsy',
'Ziyad',
'Zohodia',
'Zulafiqar',
'Zulfakar'
]

HTTP_PROXY = 'http://127.0.0.1:8118'
DOWNLOADER_MIDDLEWARES = {
     'dub.middlewares.RandomUserAgentMiddleware': 400,
     'dub.middlewares.ProxyMiddleware': 410,
     'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None
}