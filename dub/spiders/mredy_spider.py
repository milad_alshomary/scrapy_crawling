# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import FormRequest
from scrapy.selector import HtmlXPathSelector
from scrapy.conf import settings
from scrapy.http import Request
import urllib
from subprocess import call
import deathbycaptcha
from deathbycaptcha import SocketClient
import sys
import random
from dub.items import MredyItem
from multiprocessing import Process, Lock

class MredySpider(scrapy.Spider):

	handle_httpstatus_list=[404,403]

	name = "mredy"

	stop=0

	start_urls = [
		'http://www.mredy.com/?mod=login&lo=kom_60&l=ar'
	]

	captchas_in_use = [
	]

	mail_urls = [
	]
	
	urls = [
		#'http://www.mredy.com/?lo=kom_60&mod=search&search=1',
		#'http://www.mredy.com/?lo=kom_61&mod=search&search=1',
		#'http://www.mredy.com/?lo=kom_62&mod=search&search=1',
		'http://www.mredy.com/?lo=kom_63&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_64&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_65&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_66&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_67&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_68&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_69&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_70&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_71&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_72&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_73&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_74&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_75&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_76&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_77&mod=search&search=1',
		# 'http://www.mredy.com/?lo=kom_78&mod=search&search=1',
	]
	
	allowed_domains = [
		"mredy.com"
	]

	def parse(self,response) :
		yield Request(response.url,
			method   = 'post',
			headers  =  {'Content-Type' : 'application/x-www-form-urlencoded'},
			body     = urllib.urlencode({
				'M_email': 'merza.mredy@gmail.com',
				'M_password' : '123456',
				'submit': '1',
        'remeberMe': '1',
			}),
			callback = self.parse_cities
		)

	def parse_cities(self, response) :
		if 'ميرزا' in response.body :
			for url in self.urls:
				yield Request(url, callback=self.parse_city)
		else:
			print 'cant login !!' 



	def parse_city(self, response) :
		pages = response.xpath('//div[@class="pager"]').xpath('table').xpath('tr').xpath('td').extract()
		for p in range(1, len(pages) -1):
			yield Request(response.url + '&page=' + `p`, callback=self.parse_page)
			exit(1)


	def parse_page(self,response):
		for href in response.xpath('//tr[@class="tr_color"]').xpath('td[@class="photo"]').xpath('a/@href').extract():
			post_link = 'http://www.mredy.com/' + href
			yield Request(post_link, callback=self.open_mail_form)

	
	def open_mail_form(self, response):
		tools    = response.xpath('//table[@class="tools"]').xpath('tr').xpath('td');
		mail_url = tools[3].xpath('a/@href').extract()[0];
		yield Request(mail_url, callback=self.solveCaptcha)

	def solveCaptcha(self, response) :
		item = MredyItem()
		item['mail_url'] = response.url;
		#retriving captcha url
		captcha_url = response.xpath('//form[@method="post"]').xpath('//img[@id="img"]').xpath('@src').extract()[0]
		if captcha_url in self.captchas_in_use :
			with open("skipped_file.txt", "a") as skipped_file:
				skipped_file.write(response.url + '\n')
		else :
			print 'append into list ' + `len(self.captchas_in_use)`
			self.captchas_in_use.append(captcha_url)
			request = Request(captcha_url, self.reply_to_post)
			request.meta['item'] = item
			yield request


	def reply_to_post(self, response) :

		filename = response.url.split("?")[1];
		filename = '/tmp/captchs/' + filename + '.gif';
		#downloading captcha image		
		file = open(filename,'w')
		file.write(response.body)
		file.close()

		#resolving captcha text
		sClient = SocketClient('slimfadi', 'whatthebanana')
		captcha_code = sClient.decode(filename, 60) 

		print captcha_code['text']
		#reply to user
		name    = random.choice(settings.get('NAMES'))
		item = response.meta['item']
		item['captcha_url'] = response.url

		request = Request(item['mail_url'],
								method = 'post',
								headers  =  {'Content-Type' : 'application/x-www-form-urlencoded'},
								body   = urllib.urlencode({
													'customer_name' : name,
													'customer_email': name + '@gmail.com',
													'customer_phone': '',
													'customer_description': 'http://goo.gl/utptTI اعلانك مميز سوف تحصل على نتائج افضل بنشر اعلانك هنا',
													'code' : captcha_code['text'],
													'submit' : 1
												}),
								callback = self.print_to_file
							)
		request.meta['item'] = item
		yield request

	def print_to_file(self,response):
		success = response.xpath('//div[@class="success_mess"]').extract();
		if success != [] :
			print  "success : " + response.url +"\n"
		else:
			print  "failed : " + response.url + "\n"
			print  "Error: " + response.xpath('//div[@class="error_mess"]//font//text()').extract()[0].encode('utf-8')
		item = response.meta['item']
		self.captchas_in_use.remove(item['captcha_url'])


