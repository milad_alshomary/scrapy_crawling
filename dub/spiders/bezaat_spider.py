# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import FormRequest
from scrapy.selector import HtmlXPathSelector
from scrapy.conf import settings
from scrapy.http import Request
import urllib
from subprocess import call
import deathbycaptcha
from deathbycaptcha import SocketClient
import sys
import random

class BezaatSpider(scrapy.Spider):

	handle_httpstatus_list=[404,403]

	name = "bezaat"

	stop=0

	start_urls = []
	country    = "ksa"

	def __init__(self, country, city):
		self.country = country
		self.start_urls = ["http://www.bezaat.com/" + country + "/" + city]
		allowed_domains = ["http://www.bezaat.com/"]
	
	def parse(self,response) :
		for href in response.xpath('//div[@id="selectableCities"]').xpath('ul').xpath('li').xpath('a/@href').extract():
			yield Request("http://www.bezaat.com" + href, callback=self.parse_city)

	def parse_city(self,response):
		for link in response.xpath('//div[@id="rightBlock"]').xpath('//a[@class="more"]').xpath('@href').extract() :
			fullLink = "http://www.bezaat.com" + link
			yield Request(fullLink, callback=self.parse_pages)

	def parse_pages(self,response):
		for page_link in response.xpath('//ul[@class="pagination"]').xpath('li').xpath('a/@href').extract():
			page_link = "http://www.bezaat.com" + page_link
			yield Request(page_link, callback=self.parse_posts)

	def parse_posts(self, response):
		for post_link in response.xpath('//div[@class="holder rightFloat"]').xpath('h2').xpath('a/@href').extract():
			yield Request("http://www.bezaat.com" + post_link, callback=self.reply_to_post)

	def reply_to_post(self,response):
		post_id = response.url.split('/')[-1]
		name    = random.choice(settings.get('NAMES'))
		req_body = '------WebKitFormBoundaryMXBj26hsxEhNxbVp\nContent-Disposition: form-data; name="adid"\n\n' + post_id + '\n------WebKitFormBoundaryMXBj26hsxEhNxbVp\nContent-Disposition: form-data; name="replierName"\n\n' + name + '\n------WebKitFormBoundaryMXBj26hsxEhNxbVp\nContent-Disposition: form-data; name="replierEmail"\n\n' + name + '@gmail.com' + '\n------WebKitFormBoundaryMXBj26hsxEhNxbVp\nContent-Disposition: form-data; name="msg2Adv"\n\n' + 'http://goo.gl/HWra2N اعلانك ممير سوف تحصل على نتائج افضل بنشر اعلانك هنا' + '\n------WebKitFormBoundaryMXBj26hsxEhNxbVp--'
		yield Request("http://www.bezaat.com/sendemailtoadvertiser",
			method   = 'post',
			body     = req_body,
			headers  = {
				'Content-Type' : 'multipart/form-data; boundary=----WebKitFormBoundaryMXBj26hsxEhNxbVp'
			},
			callback = self.print_to_file
		)

		return

	def print_to_file(self,response):
		if "done" in response.body :
			print response.body + " : " + response.url + "\n"
		else:
			print response.body + " : " + response.url + "\n"
		return
