# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import FormRequest
from scrapy.selector import HtmlXPathSelector
from scrapy.conf import settings
from scrapy.http import Request
import urllib
from subprocess import call
import deathbycaptcha
from deathbycaptcha import SocketClient
import sys
import random

class MstamlSpider(scrapy.Spider):

	handle_httpstatus_list=[404,403]

	name = "mstaml"

	stop=0

	start_urls = [
		'http://www.mstaml.com/manage/login.php'
	]

	urls = [
		'http://www.mstaml.com/market/industries-and-trucks/0-3-0-0/?',
		'http://www.mstaml.com/market/cars-and-vehicles/0-4-0-0/?',
		'http://www.mstaml.com/market/devices-and-tools/0-7-0-0/?',
		'http://www.mstaml.com/market/estates-and-properties/0-1-0-0/?',
		'http://www.mstaml.com/market/computers-and-internet/0-5-0-0/?',
		'http://www.mstaml.com/market/computers-and-internet/0-5-0-0/?',
		'http://www.mstaml.com/market/jobs-and-services/0-9-0-0/?',
		'http://www.mstaml.com/market/trade-and-stocks/0-2-0-0/?',
		'http://www.mstaml.com/market/phones-and-numbers/0-6-0-0/?',
		'http://www.mstaml.com/market/accessories-and-misc/0-8-0-0/?'
	]
	
	allowed_domains = [
		"mstaml.com"
	]

	def parse(self,response) :
		yield Request(response.url,
			method   = 'post',
			headers  =  {'Content-Type' : 'application/x-www-form-urlencoded'},
			body     = urllib.urlencode({
				'username' : 'slimfadi',
				'password' : '123456',
				'back'     : 0,
      	'formLogin': 1
			}),
			callback = self.parse_categories
		)

	def parse_categories(self, response) :
		if 'إحصائيات' in response.body :
			for url in self.urls:
				yield Request(url, callback=self.parse_category)


	def parse_category(self, response) :
		pages = response.xpath('//form[@name="formNext"]').xpath('div').xpath('input/@value').extract()
		for p in range(0, int(pages[-1])):
			yield Request(response.url + '&p=' + `p`, callback=self.parse_page)


	def parse_page(self,response):
		for post_link in response.xpath('//div[@class="boxDarkBody dw1 gWhite ui-corner-all mb20 mt20"]').xpath('div[@class="pb3"]').xpath('a/@href').extract():
			yield Request(post_link, callback=self.reply_to_post)

	def reply_to_post(self,response):
		username = response.xpath('//a[@href="#user"]/text()').extract()[0];
		yield Request('http://www.mstaml.com/manage/internal.php?ajax=1',
			method = 'post',
			headers  =  {'Content-Type' : 'application/x-www-form-urlencoded'},
			body   = urllib.urlencode({
										'username' : username,
										'body'     : 'http://goo.gl/cdfMD3 اعلانك ممير سوف تحصل على نتائج افضل بنشر اعلانك هنا',
                    'formName' : 'internalLoggedIn'}),
			callback = self.print_to_file
		)

	def print_to_file(self,response):
		if '{"status":1}' in response.body :
			print response.body + " : " + response.url + "\n"
		else:
			print response.body + " : " + response.url + "\n"
		return
