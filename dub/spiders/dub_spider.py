# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import FormRequest
from scrapy.selector import HtmlXPathSelector
from scrapy.conf import settings
from scrapy.http import Request
import urllib
from subprocess import call
import deathbycaptcha
from deathbycaptcha import SocketClient
import sys
import random
import requests
import json

class DubSpider(scrapy.Spider):

	cookies={'fuego_algeria':'bff25e3163da0b51ec4a5d7aa45e8e0b'}

	handle_httpstatus_list=[404,403]

	name = "dub"

	stop=0

	start_urls = []
	listing    = []
	country    = 'saudi'
	
	countries_mapping = {
		'saudi' : 'sa',
		'jordan' : 'jo',
		'egypt' : 'eg',
		'kuwait' : 'kw',
		'qatar' : 'qa',
		'bahrain' : 'ba'
	}

	category_mapping = {
		'cars' : 1729,
		'items-for-sale' : 2,
		'jobs' : 46,
		'electronics-home-appliances': 1929,
		'home-garden': 1979,
		'pets': 3037,
		'clothing-accessories': 1953,
	}


	subcategory_mapping = {
		'property-for-rent' : 1727,
		'musical-instruments' : 12,
		'video-games-consoles': 1941,
		'movies-music': 12,
		'toys-collectibles': 12,
		'everything-else': 12,
		'business-industrial': 12,
		'tickets-vouchers': 0,
		'motorcycles': 1735,
		'boats': 1745,
		'cameras-imaging': 1943,
		'books': 12,
		'computers-tablets': 1933,
		'jewelry-watches': 1963,
		'baby-items': 1971,
		'sporting-goods': 12,
		'mobile-phones-accessories': 1947,
		'rooms-for-rent': 1605,
		'vacation-rentals' :1613,
		'apartments-for-rent' :1727,
		'commercial-for-rent' : 1615,
		'villas-for-rent': 1607,
		'rentals-wanted': 1615,
		'land-for-sale': 1597,
		'apartments-for-sale': 1603,
		'commercial-for-sale': 1615,
		'villas-for-sale': 1607,
		'multiple-units-for-sale': 1615,
		'marketingpr': 58,
		'business-development': 58,
		'education': 54,
		'ittelecom': 56,
		'hrrecruiting': 58,
		'secretarial': 52,
		'hospitality': 58,
		'medicalhealth': 48,
		'architectureengineering': 50,
		'executive': 58,
		'consulting': 58,
		'artdesign': 58,
		'sales': 62,
		'accounting': 60,
		'retail': 58,
		'oilgas': 58,
		'other-jobs': 58,
	}

	def __init__(self, country):
		self.country = country
		self.start_urls = ["http://" + self.country + ".dubizzle.com"]
		allowed_domains = [ self.country + ".dubizzle.com"]
		self.listings = [
			"http://"+self.country+".dubizzle.com/ar/cars/aston-martin/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/opel/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/audi/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/oullim/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/infiniti/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/acura/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/alfa-romeo/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/lotus/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/isuzu/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/proton/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/bufori/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/bugatti/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/porsche/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/buick/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/pontiac/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/bmw/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/peugeot/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/toyota/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/jaguar/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/gmc/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/jeep/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/daihatsu/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/daewoo/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/dodge/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/delorean/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/rover/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/renault/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/saab/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/ssang-yong/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/skoda/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/smart/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/subaru/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/suzuki/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/cmc/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/seat/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/citroen/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/chevrolet/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/ford/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/volvo/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/volkswagen/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/fiat/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/ferrari/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/cadillac/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/chrysler/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/car-accessories/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/kia/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/lada/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/lamborghini/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/land-rover/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/lancia/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/lexus/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/lincoln/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/mazda/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/maserati/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/mercedes-benz/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/mitsubishi/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/mercury/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/mini/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/other-make/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/nissan/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/hummer/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/honda/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/hyundai/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/bentley/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/bizzarrini/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/tata/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/rolls-royce/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/speranza/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/maybach/search/",
			"http://"+self.country+".dubizzle.com/ar/cars/mclaren/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/musical-instruments/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/video-games-consoles/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/items-wanted/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/movies-music/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/toys-collectibles/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/everything-else/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/electronics-home-appliances/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/home-garden/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/business-industrial/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/tickets-vouchers/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/pets/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/motorcycles/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/boats/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/cameras-imaging/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/books/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/computers-tablets/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/jewelry-watches/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/baby-items/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/sporting-goods/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/clothing-accessories/search/",
			"http://"+self.country+".dubizzle.com/ar/items-for-sale/mobile-phones-accessories/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-rent/rooms-for-rent/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-rent/vacation-rentals/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-rent/apartments-for-rent/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-rent/commercial-for-rent/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-rent/villas-for-rent/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-rent/rentals-wanted/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-sale/land-for-sale/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-sale/apartments-for-sale/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-sale/commercial-for-sale/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-sale/villas-for-sale/search/",
			"http://"+self.country+".dubizzle.com/ar/property-for-sale/multiple-units-for-sale/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/media/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/construction/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/bankfinance/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/marketingpr/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/business-development/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/education/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/ittelecom/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/hrrecruiting/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/secretarial/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/hospitality/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/medicalhealth/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/architectureengineering/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/executive/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/consulting/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/artdesign/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/sales/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/accounting/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/retail/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/oilgas/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/other-jobs/search/",
			"http://"+self.country+".dubizzle.com/ar/jobs/jobs-wanted/search/"
	]

	def parse(self,response) :
		yield Request("http://saudi.dubizzle.com/ar/cars/aston-martin/listing/10-listings-812708cfc0785be8a6c890f1fa900403/show/?back=", callback=self.ask_for_reply_form, cookies=self.cookies)

	def parse_listings(self,response):
		for link in self.listings:
			 for i in range(1,50):
				 yield Request(link+"?page="+str(i),callback=self.parse_listing,cookies=self.cookies)

	def parse_listing(self,response):
		for sel in response.xpath('//div[@class="u-c u-c--12o12 u-helper--no-padding-trailing u-helper--no-padding-leading"]'):
			link = 'http://'+self.country+'.dubizzle.com'+str(sel.xpath('a/@href').extract()[0])
			yield Request(link, callback=self.ask_for_reply_form, cookies=self.cookies)

	def ask_for_reply_form(self, response):
		title = response.xpath('//div[@class="u-c u-c--12o12"]').xpath('//h3').extract()[0];
		mCallback = lambda response: self.parse_post(response, title)
		yield Request(response.url.replace("show","reply"), callback=mCallback, cookies=self.cookies)

	def parse_post(self,response, title):
		csrfmiddlewaretoken = response.xpath('//input[@name="csrfmiddlewaretoken"]').xpath('@value').extract()[0]
		captcha_0           = response.xpath('//input[@name="captcha_0"]').xpath('@value').extract()[0]
		captcha_1 = self.solveCaptcha(response)
		
		parts = response.url.split('/');
		cat = parts[4]
		subcat = parts[5]
		
		#map dup categories to os categories
		cat_id = self.category_mapping.get(cat, -1)
		if(cat_id == -1) :
			cat_id = self.subcategory_mapping.get(cat, -1)

		subCat_id = self.subcategory_mapping.get(subcat, -1)
		if(subCat_id == -1) :
			subCat_id = self.category_mapping.get(subcat, -1)

		print title.decode("utf-8")
		exit(1)
		#selected_add = self.getBestAdd(response, cat_id, subCat_id)

		message = 'مرحبا, رجاء تفقد إعلاني على هذا الموقع المميز, ' + add_url
		#message = self.getBestMessage('قم%20كنب%20استخدام%20خفيف%20للبيع', cat_id, subCat_id)
		#message = 'http://goo.gl/HWra2N إعلانك مميز, ويمكنك الحصول على ما تريد من خلال زيارة السوق المفتوح'
		name    = random.choice(settings.get('NAMES'))

		if captcha_1 == 'none' :
			self.print_to_file(response, name, message, 'false', cat, subcat, 'myadd.com')
		else :
			req_body = {
				'name': name,
				'email': name + '@gmail.com',
				'message': message,
				'is_ajax' : '1',
				'csrfmiddlewaretoken':csrfmiddlewaretoken,
				'captcha_0' : captcha_0,
				'captcha_1' : captcha_1
			}
			mCallback = lambda response: self.print_to_file(response, name, message, 'true', cat, subcat, 'myadd.com')
			yield Request(response.url,
				method='post',
				body=urllib.urlencode(req_body),
				callback=mCallback,
				cookies=self.cookies
			)


	def solveCaptcha(self, response) :
		#retriving captcha url
		captcha_url = response.xpath('//img[@class="d-captcha__img"]').xpath('@src').extract()[0]
		captcha_url = 'http://'+ self.country + '.dubizzle.com/' + captcha_url
		
		#downloading captcha image
		filename = captcha_url.split("/")[-2];
		filename = '/tmp/capches/' + filename + '.png';
		file = open(filename,'wb')
		file.write(urllib.urlopen(captcha_url).read())
		file.close()

		try :
			#resolving captcha text
			sClient = SocketClient('slimfadi', 'whatthebanana')
			captcha = sClient.decode(filename, 60)
			return captcha['text'].upper()
		except : 
			return 'none'

	def print_to_file(self,response, sender, message, capcha_success, cat, subcat, add_url):
		print response.body
		result = response.xpath('//div[@class="u-alert u-alert--success"]');
		success = "true"
		if not result :
			success = "false"

		post_url = response.url.replace('/reply/' , '/show/');
		print '{"category" : "' + cat + '", "subCategory" : "'+ subcat +'", "country" : "' + self.country + '", "user_url" : "' + post_url + '", "sender" : "' + sender + '", "message" : "' + message + '" , "success":"' + success + '", "add_url":"'+ add_url +'" ,"capcha_status":"'+ capcha_success+'"}' +"\n"
		return

	def getBestAdd(query, response, category, sub_category) :
		# params = {
		# 	'q' : 'bmw',
		# 	'wt': 'json',
		# 	'defType' :'edismax',
		# 	'qf' : 'title^7%20description^3%20location^1e-13%20tag_1_name^1e-13%20tag_2_name^1%20city_name^1%20price^1&',
		# 	'bf' : 'product(recip(rord(record_posted_day),1,1000,1000),400)^30&',
		# 	'fq' : 'tag_2_id:10'
		# }

		#Retrieving post url
		fq = ''
		if(category != '-1') :
			fq+= 'tag_2_id:' + category

		if(sub_category != -1) :
			fq+=  '&tag_1_id=' + sub_category
		
		r = requests.get("http://54.76.122.44:8983/solr/collection1/select", params='q=' + query + '&wt=json&indent=true&defType=edismax&qf=title^7%20description^3%20location^1e-13%20tag_1_name^1e-13%20tag_2_name^1%20city_name^1%20price^1&bf=product(recip(rord(record_posted_day),1,1000,1000),400)^30&fq=' + fq)
		mJson = r.json()
		post_id    = mJson['response']['docs'][0]['id']
		post_title = mJson['response']['docs'][0]['title']
		post_url = 'http://' + 'sa' + '.opensooq.com/search/' + str(post_id);
		print post_url

		#Shortning the url
		body = {'longUrl' : post_url}
		params = {'key' : 'AIzaSyD1H8-OGE4TCQ7BA_3yUt1jgHQ_ViavIUM'}
		headers = {'content-type': 'application/json; charset=utf8'}
		r2  = requests.post("https://www.googleapis.com/urlshortener/v1/url", data=json.dumps(body), params=params, headers=headers)
		jsonResult = r2.json()
		post_shortend_url = jsonResult['id']

		msg = 'مرحبا, رجاء تفقد إعلاني على هذا الموقع المميز '.decode("utf-8");
		built_msg = 'hi check my post: ' + post_shortend_url
		return built_msg
