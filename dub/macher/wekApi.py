import weka.core.jvm as jvm
from weka.filters import Filter
from weka.core.classes import Random
from weka.core.converters import Loader
from weka.classifiers import Classifier, Evaluation

jvm.start(class_path=['/usr/lib/python2.7/site-packages/python_weka_wrapper-0.1.10-py2.7.egg', '/home/milad/Dev/Weka/weka-3-7-11/weka.jar'],system_cp=True, packages=True)
print 'started !!'
loader = Loader(classname="weka.core.converters.ArffLoader")
data = loader.load_file("/home/milad/Dev/Weka/weatherTest.csv.arff")
stwv = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", "last"])
stwv.set_inputformat(data)
filtered = stwv.filter(data)
print(filtered)     